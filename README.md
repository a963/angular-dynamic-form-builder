# Angular dynamic form builder
This project is about creating dynamic forms in which forms and their fields can be dynamically defined and configured. We can set field settings such as name, label, type, description and so on. also we can define users groups than have different access on different forms and fields. 

## ToDo list:
1. implement user Auth service and Auth guard
2. implement form view,edit,delete
3. implement users group edit,delete

## Development server

Clone project files.
Run `npm install` to install project packages.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
