// this component is used to show message to users and show loading bar when applicatin makes a request.
import { Component, OnInit } from '@angular/core';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss']
})
export class MessageBoxComponent implements OnInit {
  messageBox: any;
  count = 0;

  constructor(private messageBoxService: MessageService) {
    messageBoxService.messageBox.subscribe((data) => {
      this.messageBox = data;
      this.show();
    });
    // check if  count > 0 show loader bar 
    messageBoxService.loading.subscribe((data) => {
      data === '+' ? this.count++ : this.count--;
    });
  }
// shows message box for 3 second
  show(): void {
    setTimeout(() => {
      this.messageBox.type = 'hidden';
      setTimeout(() => {
        this.messageBox = {};
      }, 900);
    }, 3000);
  }

  ngOnInit(): void {
  }

}
