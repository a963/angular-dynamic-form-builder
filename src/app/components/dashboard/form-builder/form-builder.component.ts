import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MessageService } from 'src/app/services/message.service';
import { FieldFormDialogComponent } from '../field-form-dialog/field-form-dialog.component';
@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
})
export class FormBuilderComponent implements OnInit {
  buildForm: FormGroup;
  invalidSubmit = false;
  fields = [];
  usersGroups;
  filteredUserGroups = [];
  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private msgService: MessageService
  ) {
    this.createBuildForm();
    this.usersGroups = JSON.parse(localStorage.getItem('groups'));
    this.filteredUserGroups = this.usersGroups;
  }

  ngOnInit(): void { }
  // initializes form
  createBuildForm(): void {
    this.buildForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      users: [[], [Validators.required]],
    });
  }
  // filters users groups with searched value
  filter(searchValue) {
    this.filteredUserGroups = [];
    for (let i = 0, il = this.usersGroups.length; i < il; i++) {
      if (this.usersGroups[i].name.includes(searchValue)) {
        this.filteredUserGroups.push(this.usersGroups[i]);
      }
    }
  }
  // opens dialog box that contains field form
  openDialog(): void {
    const dialogRef = this.dialog.open(FieldFormDialogComponent, {
      minHeight: '390px',
    });
    // after closing dialog box, gets field form data
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.fields.push(result);
      }
    });
  }
  addForm(formDirective) {
    if (this.buildForm.invalid) {
      this.invalidSubmit = true;
      return this.msgService.showMessageBox({
        message: 'please fill the form correctly',
        type: 'error',
      });
    }
    this.invalidSubmit = false;
    this.msgService.showMessageBox({
      message: 'Form saved successfully',
      type: 'success',
    });
    // now we should use request servic,to add form to server
    // but for developement, i am using local storage
    let values = this.buildForm.value;
    values.fields = this.fields;
    let forms = JSON.parse(localStorage.getItem('forms'));
    // Checks to see if forms already exists or initialize forms
    forms ? forms.push(values) : (forms = [values]);
    localStorage.setItem('forms', JSON.stringify(forms));
    formDirective.resetForm();
    this.buildForm.reset();
    this.fields = [];
  }
}
