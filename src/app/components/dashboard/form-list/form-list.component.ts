// this component is for listing created forms.
// todo: implement view,edit,delete forms
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-list',
  templateUrl: './form-list.component.html',
  styleUrls: ['./form-list.component.scss'],
})
export class FormListComponent implements OnInit {
  forms = [];
  constructor() {
    this.forms = JSON.parse(localStorage.getItem('forms'));
  }

  ngOnInit(): void { }
}
