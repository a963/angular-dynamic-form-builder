// this component is field form dialog box
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'src/app/services/message.service';
export interface OptionItem {
  name: string;
  show: boolean;
}
@Component({
  selector: 'app-field-form-dialog',
  templateUrl: './field-form-dialog.component.html',
  styleUrls: ['./field-form-dialog.component.scss'],
})
export class FieldFormDialogComponent implements OnInit {
  fieldForm: FormGroup;
  usersGroups;
  filteredUserGroups = [];

  constructor(
    public dialogRef: MatDialogRef<FieldFormDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private msgService: MessageService
  ) {
    this.createFieldForm();
    // gets users groups from local storage
    this.usersGroups = JSON.parse(localStorage.getItem('groups'));
    this.filteredUserGroups = this.usersGroups;
  }
  ngOnInit() { }
  // initializes field form
  createFieldForm(): void {
    this.fieldForm = this.formBuilder.group({
      name: ['', Validators.required],
      label: ['', Validators.required],
      type: ['', Validators.required],
      description: [''],
      pattern: [''],
      format: [''],
      users: [[], Validators.required],
      isRequired: [false],
    });
  }
  // filters users groups with searched value
  filter(searchValue) {
    this.filteredUserGroups = [];
    for (let i = 0, il = this.usersGroups.length; i < il; i++) {
      if (this.usersGroups[i].name.includes(searchValue)) {
        this.filteredUserGroups.push(this.usersGroups[i]);
      }
    }
  }
  // close dialog box
  addField() {
    // if form was not valid, shows error and field will not add untill form get valid
    if (this.fieldForm.invalid) {
      return this.msgService.showMessageBox({
        message: 'please fill the form correctly',
        type: 'error',
      });
    }
    this.dialogRef.close(this.fieldForm.value);
  }
}
