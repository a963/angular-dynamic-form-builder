import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersGroupsDialogComponent } from './users-groups-dialog.component';

describe('UsersGroupsDialogComponent', () => {
  let component: UsersGroupsDialogComponent;
  let fixture: ComponentFixture<UsersGroupsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersGroupsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersGroupsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
