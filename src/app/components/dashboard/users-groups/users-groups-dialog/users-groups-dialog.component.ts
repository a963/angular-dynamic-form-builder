// this component is users group form dialog box
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MessageService } from 'src/app/services/message.service';
export interface OptionItem {
  name: string;
  show: boolean;
}
@Component({
  selector: 'app-users-groups-dialog',
  templateUrl: './users-groups-dialog.component.html',
  styleUrls: ['./users-groups-dialog.component.scss']
})
export class UsersGroupsDialogComponent implements OnInit {
  userForm: FormGroup;
  users;
  filteredUsers;

  constructor(
    public dialogRef: MatDialogRef<UsersGroupsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private formBuilder: FormBuilder,
    private msgService: MessageService
  ) {
    this.createUserGourpForm();
    this.users = JSON.parse(localStorage.getItem('users'))
    this.filteredUsers = this.users;
  }
  ngOnInit() { }
  // initializes user group form
  createUserGourpForm(): void {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.required],
      users: [[], Validators.required],
      formAccess: [[], Validators.required],
      fieldAccess: [[], Validators.required],
    });
  }
  // filters users groups with searched value
  filter(searchValue) {
    this.filteredUsers = [];
    for (let i = 0, il = this.users.length; i < il; i++) {
      if (this.users[i].name.includes(searchValue)) {
        this.filteredUsers.push(this.users[i]);
      }
    }
  }
  submit() {
    // if form was not valid, shows error and user group will not add untill form get valid
    if (this.userForm.invalid) {
      return this.msgService.showMessageBox({
        message: 'please fill the form correctly',
        type: 'error',
      });
    }
    // now we should use request servic,to add users group to server
    // but for developement, i am using local storage
    let values = this.userForm.value;
    let groups = JSON.parse(localStorage.getItem('groups'));
    // Checks to see if users group already exists or initialize users group
    groups ? groups.push(values) : (groups = [values]);
    localStorage.setItem('groups', JSON.stringify(groups));
    this.dialogRef.close(this.userForm.value);
    this.msgService.showMessageBox({
      message: 'user group added successfully',
      type: 'success',
    });
  }
}
