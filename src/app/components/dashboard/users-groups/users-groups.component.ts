// this componet is for making new user groups and set group access on forms and fields.
// todo: implement view,edit,delete groups.
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsersGroupsDialogComponent } from './users-groups-dialog/users-groups-dialog.component';

@Component({
  selector: 'app-users-groups',
  templateUrl: './users-groups.component.html',
  styleUrls: ['./users-groups.component.scss']
})
export class UsersGroupsComponent implements OnInit {
  groups = [];
  constructor(public dialog: MatDialog) {
    this.groups = JSON.parse(localStorage.getItem('groups'))
  }

  ngOnInit(): void {
  }
  // opens dialog box that contains users group form
  openDialog(): void {
    const dialogRef = this.dialog.open(UsersGroupsDialogComponent);
    // after closing dialog box, gets users group form data
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.groups ? this.groups.push(result) : this.groups = [result];
      }
    });
  }
}
