import { Component, OnInit } from '@angular/core';
import { trigger, style, animate, transition, state } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0, width: 0 }),
            animate('0.3s ease-out',
              style({ opacity: 1, width: '*', display: 'none' }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ width: '*' }),
            animate('0.3s ease-out',
              style({ opacity: 1, width: 0 }))
          ]
        ),
      ]
    )
  ]
})
export class DashboardComponent implements OnInit {
  isExpanded = false;
  showFiller = false;
  activeRoute = 1;
  routes = { dashboard: 1, 'form-builder': 2, 'built-forms': 3, 'users-groups': 4 }
  ngOnInit(): void {
  }
  constructor(private router: Router) {
    // checks which route is active to set active class
    const splitedUrl = this.router.url.split('/')
    this.activeRoute = this.routes[splitedUrl[splitedUrl.length - 1]]
  }
  navigate(route): void {
    this.router.navigateByUrl(`${route}`);
  }
}
