// this component is for login page
// todo: implenet authservice and authGuard
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/services/message.service';
import { RequestService } from 'src/app/services/request.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private msgService: MessageService, private req: RequestService, private router: Router) {
    this.createLoginForm();
  }

  ngOnInit(): void {
  }
  // initializing login form
  createLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }
  // this is just for test and is not a real life example
  // login with 'admin@admin' as email and 'admin' as password
  login(): void {
    // validates form
    if (this.loginForm.invalid) {
      return this.msgService.showMessageBox({ message: 'please fill the form correctly', type: 'error' })
    }
    // now we should make a request and send data to server to check id email and password is correct or not
    // because we didnt implemented a backend server we just login
    this.msgService.load('+')
    setTimeout(() => {
      this.msgService.load('')
      this.router.navigateByUrl('dashboard');
      this.msgService.showMessageBox({message:'login successful',type:'success'})
    },3000)


    // Request to api example :
    // this.req.post('/api/Account/Login', this.loginForm.value, {}, (err, data) => {
    //   if (err) {
    //     return this.msgService.showMessageBox({ message: 'username or password is wrong', type: 'error' });
    //   }
    //   localStorage.setItem('accessToken', data.accessToken);
    //   localStorage.setItem('refreshToken', data.refreshToken);
    //   this.router.navigateByUrl('dashboard');
    // }
  }
}
