import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dynamic-form-builder';
  constructor() {
    // Set default users to local storage
    localStorage.setItem('users', JSON.stringify([
      { name: 'john', id: 1 },
      { name: 'james', id: 2 },
      { name: 'harper', id: 3 },
      { name: 'mason', id: 4 },
      { name: 'ella', id: 5 },
      { name: 'jackson', id: 6 },
      { name: 'scarlett', id: 7 },
      { name: 'madison', id: 8 },
      { name: 'ellie', id: 9 },
      { name: 'julian', id: 10 },
    ]))
  }
}
