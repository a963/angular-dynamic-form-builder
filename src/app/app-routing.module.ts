import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormBuilderComponent } from './components/dashboard/form-builder/form-builder.component';
import { FormListComponent } from './components/dashboard/form-list/form-list.component';
import { UsersGroupsComponent } from './components/dashboard/users-groups/users-groups.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [
      { path: 'form-builder', component: FormBuilderComponent },
      { path: 'form-list', component: FormListComponent },
      { path: 'users-groups', component: UsersGroupsComponent },
    ],
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
