// this service is used to display application messages to users
import { EventEmitter, Injectable } from '@angular/core';

interface Message {
  message: string; //the messages displayed to the user
  type: string; //type of message, it can be 'success'(green color),'error'(red color),'warn'(yellow color)
}

@Injectable({
  providedIn: 'root'
})

export class MessageService {
  messageBox: EventEmitter<Message> = new EventEmitter();
  loading: EventEmitter<string> = new EventEmitter();

  constructor() {
  }
// emit passed data to show message box
  showMessageBox(data: Message): void {
    this.messageBox.emit(data);
  }
// adds loader bar count
  load(value: string): void {
    this.loading.emit(value);
  }

}
