// this service is used to make http requests.
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private url = "https://example.com";// api server base url

  constructor(private http: HttpClient, private messageService: MessageService) {
  }
  // this property is used to make requests with GET Method
  get(path: string, options, cb): any {
    this.messageService.load('+');// call this property to display loading bar
    if (options.headers) {
      options = {
        headers: new HttpHeaders(options.headers),
      };
    }
    this.http.get(this.url + path, options).subscribe(response => {
      this.messageService.load('');// call this property to stop displaying loading bar
      cb(null, response);
    }, error => {
      this.messageService.load('');// call this property to stop displaying loading bar
      cb(error);
    });
  }
  // this property is used to make requests with POST Method
  post(path: string, data: any, options, cb): any {
    this.messageService.load('+');
    this.http.post(this.url + path, data, options).subscribe(response => {
      this.messageService.load('');
      cb(null, response);
    }, error => {
      this.messageService.load('');
      cb(error);
    });
  }
}
