import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './components/login/login.component';
import { MessageBoxComponent } from './components/message-box/message-box.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormBuilderComponent } from './components/dashboard/form-builder/form-builder.component';
import { FormListComponent } from './components/dashboard/form-list/form-list.component';
import { UsersGroupsComponent } from './components/dashboard/users-groups/users-groups.component';
import { FieldFormDialogComponent } from './components/dashboard/field-form-dialog/field-form-dialog.component';
import { UsersGroupsDialogComponent } from './components/dashboard/users-groups/users-groups-dialog/users-groups-dialog.component';
import { AngularMaterialModule } from './material.module'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MessageBoxComponent,
    DashboardComponent,
    FormBuilderComponent,
    FormListComponent,
    UsersGroupsComponent,
    FieldFormDialogComponent,
    UsersGroupsDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
